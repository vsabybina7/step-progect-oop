const creatButton = document.querySelector('.creatButton')

// Создание Div для формы
let coverDiv = document.createElement('div');
coverDiv.id = 'visit';

// Крестик для удаления
let cross = document.createElement('div');
cross.className = 'cross visitCross'
let crossImg = document.createElement('img');
crossImg.setAttribute('src', "img/cross.png");
crossImg.setAttribute('alt', "cross");
cross.appendChild(crossImg)
coverDiv.appendChild(cross);
// Создание форму
let form = document.createElement("form");
form.setAttribute('method', "post");
form.id = 'form';

// Создание окна с перечнем докторов
let paragraph1 = document.createElement('p')
let label = document.createElement('label')
label.for = 'doctor'
label.innerText = 'Доктор'
label.className = 'label'
paragraph1.appendChild(label)

let inputDoctorName = document.createElement("select");
inputDoctorName.className = "visitInput"
inputDoctorName.id = "sel"
let selectOption0 = document.createElement("option");
selectOption0.innerText = 'Выберети специалиста'
// selectOption0.setAttribute('selected', 'true')
selectOption0.setAttribute('disabled', 'true')
inputDoctorName.appendChild(selectOption0)
let selectOption1 = document.createElement("option");
selectOption1.innerText = 'Кардиолог'
inputDoctorName.appendChild(selectOption1)
let selectOption2 = document.createElement("option");
selectOption2.innerText = 'Стоматолог'
inputDoctorName.appendChild(selectOption2)
let selectOption3 = document.createElement("option");
selectOption3.innerText = 'Терапевт'
inputDoctorName.appendChild(selectOption3)
paragraph1.appendChild(inputDoctorName)

// Создание окна с датой визита
let paragraph2 = document.createElement('p')
let label1 = document.createElement('label')
label1.for = 'date'
label1.innerText = 'Дата визита'
label1.className = 'label'
paragraph2.appendChild(label1)
let inputDateOfVisit = document.createElement("input");
inputDateOfVisit.type = "date";
inputDateOfVisit.name = 'date'
inputDateOfVisit.required = true
inputDateOfVisit.className = "visitInput";
paragraph2.appendChild(inputDateOfVisit)
// Создание окна с возрастом
let paragraph3 = document.createElement('p')
let label2 = document.createElement('label')
label2.for = 'date'
label2.innerText = 'Возраст'
label2.className = 'label'
paragraph3.appendChild(label2)
let inputAge = document.createElement("input");
inputAge.type = "text";
inputAge.required = true
inputAge.name = "age";
inputAge.className = "visitInput";
inputAge.placeholder = 'Возраст'
paragraph3.appendChild(inputAge)
// Создание окна с ФИО
let paragraph4 = document.createElement('p')
let label3 = document.createElement('label')
label3.for = 'date'
label3.innerText = 'ФИО'
label3.className = 'label'
paragraph4.appendChild(label3)
let inputPatientName = document.createElement("input");
inputPatientName.type = "text";
inputPatientName.required = true
inputPatientName.name = "patientName";
inputPatientName.className = "visitInput";
inputPatientName.placeholder = 'ФИО'
paragraph4.appendChild(inputPatientName)
// Создание окна с Коментариями
let paragraph5 = document.createElement('p')
let label4 = document.createElement('label')
label4.for = 'date'
label4.innerText = 'Коментарии'
label4.className = 'label'
paragraph5.appendChild(label4)
let comments = document.createElement("textarea");
comments.className = "visitInput";
comments.placeholder = 'Коментарии'
paragraph5.appendChild(comments)
// Создание кнопки Создать
let paragraph6 = document.createElement('p')
let inputCreat = document.createElement("button");
inputCreat.type = "button";
inputCreat.id = "btnCreat";
inputCreat.innerText = 'Создать'
paragraph6.appendChild(inputCreat)
// Создание окна для Кардиолога 'Обычное давление'
let paragraph7 = document.createElement('p')
let label7 = document.createElement('label')
label7.for = 'doctor'
label7.innerText = 'Давление'
label7.className = 'label'
paragraph7.appendChild(label7)
let inputPressure = document.createElement("input");
inputPressure.type = "text";
inputPressure.required = true
inputPressure.name = "pressure";
inputPressure.className = "visitInput";
inputPressure.placeholder = 'Обычное давление'
paragraph7.appendChild(inputPressure)
// Создание окна для Кардиолога 'Индекс массы тела'
let paragraph8 = document.createElement('p')
let label8 = document.createElement('label')
label8.for = 'doctor'
label8.innerText = 'Масса тела'
label8.className = 'label'
paragraph8.appendChild(label8)
let inputBodyMass = document.createElement("input");
inputBodyMass.type = "text";
inputBodyMass.required = true
inputBodyMass.name = "bodyMass";
inputBodyMass.className = "visitInput";
inputBodyMass.placeholder = 'Индекс массы тела'
paragraph8.appendChild(inputBodyMass)
// Создание окна для Кардиолога 'Перенесенные заболевания серд.-сосуд. системы'
let paragraph9 = document.createElement('p')
let label9 = document.createElement('label')
label9.for = 'doctor'
label9.innerText = 'Заболевания'
label9.className = 'label'
paragraph9.appendChild(label9)
let inputHeartDisease = document.createElement("input");
inputHeartDisease.type = "text";
inputHeartDisease.required = true
inputHeartDisease.name = "bodyMass";
inputHeartDisease.className = "visitInput";
inputHeartDisease.placeholder = 'Перенесенные заболевания серд.-сосуд. системы'
paragraph9.appendChild(inputHeartDisease)
// Создание окна для Стоматолога 'Дата последнего посещения'
let paragraph10 = document.createElement('p')
let label10 = document.createElement('label')
label10.for = 'doctor'
label10.innerText = 'Дата последнего посещения'
label10.className = 'label'
paragraph10.appendChild(label10)
let inputLastVisit = document.createElement("input");
inputLastVisit.type = "date";
inputLastVisit.required = true
inputLastVisit.name = "lastVisit";
inputLastVisit.className = "visitInput";
inputLastVisit.placeholder = 'Дата последнего посещения'
paragraph10.appendChild(inputLastVisit)


function creatCardiologist() {
    document.body.appendChild(coverDiv);
    form.appendChild(paragraph2);
    form.appendChild(paragraph3);
    form.appendChild(paragraph4);
    form.appendChild(paragraph7);
    form.appendChild(paragraph8);
    form.appendChild(paragraph9);
    form.appendChild(paragraph5);
    form.appendChild(paragraph6);
    document.getElementsByTagName('body')[0].appendChild(coverDiv).appendChild(form)
}

function creatDentist() {
    document.body.appendChild(coverDiv);
    form.appendChild(paragraph2);
    form.appendChild(paragraph3);
    form.appendChild(paragraph4);
    form.appendChild(paragraph10);
    form.appendChild(paragraph5);
    form.appendChild(paragraph6);
    document.getElementsByTagName('body')[0].appendChild(coverDiv).appendChild(form)
}

function creatTherapist() {
    document.body.appendChild(coverDiv);
    form.appendChild(paragraph2);
    form.appendChild(paragraph3);
    form.appendChild(paragraph4);
    form.appendChild(paragraph5);
    form.appendChild(paragraph6);
    document.getElementsByTagName('body')[0].appendChild(coverDiv).appendChild(form)
}

function cleanForm () {
    let childNodes = [inputDateOfVisit, inputAge, inputPatientName, inputPressure, inputBodyMass, inputHeartDisease, inputLastVisit]
    for (let i = 0; i < childNodes.length; i++) {
        if (childNodes[i].type === 'text' || childNodes[i].type === 'date'){
            childNodes[i].value = '';
        }
    }
    comments.value = ''
}

function creatVisitForm() {
    if(coverDiv.style.display === 'none'){
        coverDiv.style.display = "block";
    }
    form.appendChild(paragraph1);
    form.appendChild(paragraph6);
    coverDiv.appendChild(form);
    document.body.appendChild(coverDiv);
}

function removeAllInput() {
    paragraph2.remove();
    paragraph3.remove();
    paragraph4.remove();
    paragraph5.remove();
    paragraph7.remove();
    paragraph8.remove();
    paragraph9.remove();
    paragraph10.remove();
    // form.remove(paragraph6);
}

function creatDoctorCard() {
    inputDoctorName.addEventListener('change', function () {
        console.log(inputDoctorName.value)
            removeAllInput()
        if (inputDoctorName.value === 'Кардиолог') {
            creatCardiologist()
        } else if (inputDoctorName.value === 'Стоматолог') {
            creatDentist()
        } else if (inputDoctorName.value === 'Терапевт') {
            creatTherapist()
        }
    })
}

class Visitor {
    constructor() {
        this._visitName = inputDoctorName.value
        this._dateOfVisit = inputDateOfVisit.value
        this._patientName = inputPatientName.value
        this._age = inputAge.value
        this._comments = comments.value
    }

    openForm() {
            creatVisitForm()
            cleanForm ()
            removeAllInput()
            selectOption0.selected = 'selected'
    }
    chooseDoctor() {
        form.remove(paragraph6)
        inputDoctorName.addEventListener('click', function () {
            creatDoctorCard()
        })
    }
    closeForm() {
        cross.addEventListener('click', () => {
            coverDiv.remove()

        })
    }

    creatPatientCard () {
        creatDivForCard(generalDoctor)
    }

}

let doctor = new Visitor()
doctor.closeForm();
doctor.chooseDoctor()
creatButton.addEventListener('click', () => { doctor.openForm() });

class Cardiologist extends Visitor {
    constructor() {
        super()
        this._inputPressure = inputPressure.value
        this._inputBodyMass = inputBodyMass.value
        this._inputHeartDisease = inputHeartDisease.value
    }
    openForm() {
        super.openForm()
    }
    chooseDoctor() {
        super.openForm()
    }
    closeForm() {
        super.openForm()
    }
    creatPatientCard () {
        super.creatPatientCard()
    }
}
class Dentist extends Visitor {
    constructor() {
        super()
        this._inputLastVisit = inputLastVisit.value
    }
    openForm() {
        super.openForm()
    }
    chooseDoctor() {
        super.openForm()
    }
    closeForm() {
        super.openForm()
    }
    creatPatientCard () {
        super.creatPatientCard()
    }
}
class Therapist extends Visitor {
    constructor() {
        super()
    }
    openForm() {
        super.openForm()
    }
    chooseDoctor() {
        super.openForm()
    }
    closeForm() {
        super.openForm()
    }
    creatPatientCard () {
        super.creatPatientCard()
    }
}

let d = document.querySelector('.backgroundContainer')
let array = []

window.onload = function () {
    if (localStorage.getItem('generalDoctor')) {
        array = JSON.parse(localStorage.getItem('generalDoctor'))
        array.forEach(function (generalDoctor) {
            creatDivForCard(generalDoctor)
        })
        let backgroundContainerText = document.getElementById('backgroundContainerText')
        if (array.length){
            backgroundContainerText.style.display = 'none'
        }
    }

}

function creatDivForCard(generalDoctor) {
    let divForCard = document.createElement('div')
    divForCard.className = 'divForCard'
    let crossImg2 = document.createElement('img');
    let inputShowMore = document.createElement('div')
    // divForCard.setAttribute('draggable', 'true');
    crossImg2.setAttribute('src', "img/cross.png");
    crossImg2.setAttribute('alt', "cross");
    crossImg2.className = 'cross2'
    divForCard.appendChild(crossImg2);

    let p_0 = document.createElement('p')
    p_0.className = 'p_0'
    p_0.innerText = `Врач: ${generalDoctor._visitName}`
    divForCard.appendChild(p_0)
    let p_1 = document.createElement('p')
    p_1.className = 'p_0'
    p_1.innerText = `ФИО: ${generalDoctor._patientName}`
    let p_2 = document.createElement('p')
    p_2.className = 'p'
    p_2.innerText = `Дата визита: ${generalDoctor._dateOfVisit}`
    let p_3 = document.createElement('p')
    p_3.className = 'p'
    p_3.innerText = `Возраст: ${generalDoctor._age} лет`
    let p_4 = document.createElement('p')
    p_4.className = 'p'
    p_4.innerText = `Коментарии: ${generalDoctor._comments}`
    let p_5 = document.createElement('p')
    p_5.className = 'p'
    p_5.innerText = `Давление: ${generalDoctor._inputPressure}`
    let p_6 = document.createElement('p')
    p_6.className = 'p'
    p_6.innerText = `Масса тела: ${generalDoctor._inputBodyMass}`
    let p_7 = document.createElement('p')
    p_7.className = 'p'
    p_7.innerText = `Заболевания: ${generalDoctor._inputHeartDisease}`
    let p_8 = document.createElement('p')
    p_8.className = 'p'
    p_8.innerText = `Последний визит: ${generalDoctor._inputLastVisit}`

    inputShowMore.className = 'inputShowMore'
    inputShowMore.innerText = 'Показать больше'
    divForCard.appendChild(inputShowMore);
    d.appendChild(divForCard)

    makeDragonDrop(divForCard);

    if (inputDoctorName.value === 'Выберети специалиста') {
        divForCard.remove()
        p_2.style.display = 'none'
        p_3.style.display = 'none'
        p_4.style.display = 'none'
        p_5.style.display = 'none'
        p_6.style.display = 'none'
        p_7.style.display = 'none'
        p_8.style.display = 'none'
        alert('Выбирите доктора!')
    } else if (inputDoctorName.value === 'Кардиолог') {
        divForCard.appendChild(p_1)
        divForCard.appendChild(p_2)
        divForCard.appendChild(p_3)
        divForCard.appendChild(p_5)
        divForCard.appendChild(p_6)
        divForCard.appendChild(p_7)
        divForCard.appendChild(p_4)
    } else if (inputDoctorName.value === 'Стоматолог') {
        divForCard.appendChild(p_0)
        divForCard.appendChild(p_1)
        divForCard.appendChild(p_2)
        divForCard.appendChild(p_3)
        divForCard.appendChild(p_8)
        divForCard.appendChild(p_4)
    } else if (inputDoctorName.value === 'Терапевт') {
        divForCard.appendChild(p_1)
        divForCard.appendChild(p_2)
        divForCard.appendChild(p_3)
        divForCard.appendChild(p_4)
    }
    function showMore () {
        if (divForCard.style.paddingBottom === '') {
            divForCard.style.paddingBottom = '320px'
            p_1.style.display = 'block'
            p_2.style.display = 'block'
            p_3.style.display = 'block'
            p_5.style.display = 'block'
            p_6.style.display = 'block'
            p_7.style.display = 'block'
            p_8.style.display = 'block'
            p_4.style.display = 'block'
        } else {
            divForCard.style.paddingBottom = ''
            p_2.style.display = 'none'
            p_3.style.display = 'none'
            p_4.style.display = 'none'
            p_5.style.display = 'none'
            p_6.style.display = 'none'
            p_7.style.display = 'none'
            p_8.style.display = 'none'
        }
    }
    inputShowMore.addEventListener('click', () => {
        showMore ()
    })

    crossImg2.addEventListener('click', () => {
        divForCard.remove()
        let card = document.querySelectorAll('.divForCard');
        let currentIndex = 0;
        card.forEach(function (element, index) {
            if (event.target.parentNode === element)
                currentIndex = index;

        });

        array.splice(currentIndex, 1);

        localStorage.setItem('generalDoctor', JSON.stringify(array));
        if (array.length === 0) {
            let backgroundContainerText = document.getElementById('backgroundContainerText')
            backgroundContainerText.style.display = 'block'
        }
    })
// localStorage.setItem(`${Math.random()}`,divForCard.outerHTML);
}

inputCreat.addEventListener('click', () => {
    let generalDoctor;
    coverDiv.style.display = 'none'
    let backgroundContainerText = document.getElementById('backgroundContainerText')
    backgroundContainerText.innerText = ''
    if (inputDoctorName.value === 'Выберети специалиста') {
        removeAllInput()
        coverDiv.style.display = 'block'
    } else if (inputDoctorName.value === 'Кардиолог') {
        generalDoctor = new Cardiologist()
    } else if (inputDoctorName.value === 'Стоматолог') {
        generalDoctor = new Dentist()
    } else if (inputDoctorName.value === 'Терапевт') {
        generalDoctor = new Therapist()
    }
    array.push(generalDoctor);
    localStorage.setItem('generalDoctor',JSON.stringify(array));
    console.log(array);
    creatDivForCard(generalDoctor);

    window.onload = function () {
        if (localStorage.getItem('generalDoctor')){
            array = JSON.parse(localStorage.getItem('generalDoctor'));
                    console.log(array);
                    array.forEach(function (generalDoctor) {
                        creatDivForCard(generalDoctor)
                    })
                }
    };
})
//*************** реализация Drag’n’Drop ******************//
findCard();

function makeDragonDrop(cardTarget) {
    let card = cardTarget;
    let desk = document.querySelector('.backgroundContainer');

    function move(e) {
        let cord = card.getBoundingClientRect();
        let dek = desk.getBoundingClientRect();
        if ((cord.x - 20- dek.x) < 0) {
            card.mousePositionX = e.clientX + card.offsetLeft - 30;
        }
        if ((cord.y - dek.y) < 0) {
            card.mousePositionY = e.clientY;
        }
        if (((dek.x + dek.width) - (cord.x + cord.width + 20)) < 0) {
            card.mousePositionX = (card.offsetLeft + cord.width - dek.width) + e.clientX + 30;
        }
        if (((dek.y + dek.height) - (cord.y + cord.height)) < 0) {
            card.mousePositionY = (dek.height) - e.clientY;
        }
        card.style.transform = `translate(${e.clientX - card.mousePositionX}px,${e.clientY - card.mousePositionY}px)`;
    }

    card.addEventListener('mousedown', (e) => {
        if (card.style.transform) {
            const transforms = card.style.transform;
            const transformX = parseFloat(transforms.split('(')[1].split(',')[0]);
            const transformY = parseFloat(transforms.split('(')[1].split(',')[1]);
            card.mousePositionX = e.clientX - transformX;
            card.mousePositionY = e.clientY - transformY;
        } else {
            card.mousePositionX = e.clientX;
            card.mousePositionY = e.clientY;
        }
        document.addEventListener('mousemove', move);
    });

    document.addEventListener('mouseup', () => {
        document.removeEventListener('mousemove', move);

        return false;
    });
}

function findCard() {
    if (document.querySelectorAll('.divForCard').length) {
        document.querySelectorAll('.divForCard').forEach((item) => {
            makeDragonDrop(item);
        })
    }
}





