
let d = document.querySelector('.backgroundContainer')
const array = []


function creatDivForCard(generalDoctor) {
    let divForCard = document.createElement('div')
    divForCard.className = 'divForCard'
    let crossImg2 = document.createElement('img');
    let inputShowMore = document.createElement('div')
    crossImg2.setAttribute('src', "img/cross.png");
    crossImg2.setAttribute('alt', "cross");
    crossImg2.className = 'cross2'
    divForCard.appendChild(crossImg2);

    let p_0 = document.createElement('p')
    p_0.className = 'p_0'
    p_0.innerText = `Врач: ${generalDoctor._visitName}`
    divForCard.appendChild(p_0)
    let p_1 = document.createElement('p')
    p_1.className = 'p_0'
    p_1.innerText = `ФИО: ${generalDoctor._patientName}`
    let p_2 = document.createElement('p')
    p_2.className = 'p'
    p_2.innerText = `Дата визита: ${generalDoctor._dateOfVisit}`
    let p_3 = document.createElement('p')
    p_3.className = 'p'
    p_3.innerText = `Возраст: ${generalDoctor._age} лет`
    let p_4 = document.createElement('p')
    p_4.className = 'p'
    p_4.innerText = `Коментарии: ${generalDoctor._comments}`
    let p_5 = document.createElement('p')
    p_5.className = 'p'
    p_5.innerText = `Давление: ${generalDoctor._inputPressure}`
    let p_6 = document.createElement('p')
    p_6.className = 'p'
    p_6.innerText = `Масса тела: ${generalDoctor._inputBodyMass}`
    let p_7 = document.createElement('p')
    p_7.className = 'p'
    p_7.innerText = `Заболевания: ${generalDoctor._inputHeartDisease}`
    let p_8 = document.createElement('p')
    p_8.className = 'p'
    p_8.innerText = `Последний визит: ${generalDoctor._inputLastVisit}`

    inputShowMore.className = 'inputShowMore'
    inputShowMore.innerText = 'Показать больше'
    divForCard.appendChild(inputShowMore);
    d.appendChild(divForCard)

    if (inputDoctorName.value === 'Выберети специалиста') {
        divForCard.remove()
        p_2.style.display = 'none'
        p_3.style.display = 'none'
        p_4.style.display = 'none'
        p_5.style.display = 'none'
        p_6.style.display = 'none'
        p_7.style.display = 'none'
        p_8.style.display = 'none'
        alert('Выбирите доктора!')
    } else if (inputDoctorName.value === 'Кардиолог') {
        divForCard.appendChild(p_1)
        divForCard.appendChild(p_2)
        divForCard.appendChild(p_3)
        divForCard.appendChild(p_5)
        divForCard.appendChild(p_6)
        divForCard.appendChild(p_7)
        divForCard.appendChild(p_4)
    } else if (inputDoctorName.value === 'Стоматолог') {
        divForCard.appendChild(p_0)
        divForCard.appendChild(p_1)
        divForCard.appendChild(p_2)
        divForCard.appendChild(p_3)
        divForCard.appendChild(p_8)
        divForCard.appendChild(p_4)
    } else if (inputDoctorName.value === 'Терапевт') {
        divForCard.appendChild(p_1)
        divForCard.appendChild(p_2)
        divForCard.appendChild(p_3)
        divForCard.appendChild(p_4)
    }
    function showMore () {
        if (divForCard.style.paddingBottom === '') {
            divForCard.style.paddingBottom = '320px'
            p_1.style.display = 'block'
            p_2.style.display = 'block'
            p_3.style.display = 'block'
            p_5.style.display = 'block'
            p_6.style.display = 'block'
            p_7.style.display = 'block'
            p_8.style.display = 'block'
            p_4.style.display = 'block'
        } else {
            divForCard.style.paddingBottom = ''
            p_2.style.display = 'none'
            p_3.style.display = 'none'
            p_4.style.display = 'none'
            p_5.style.display = 'none'
            p_6.style.display = 'none'
            p_7.style.display = 'none'
            p_8.style.display = 'none'
        }
    }
    inputShowMore.addEventListener('click', () => {
        showMore ()
    })
    // function (generalDoctor) {
    //     if (localStorage.setItem('generalDoctor')) {
    //         array = JSON.parse(localStorage.setItem('generalDoctor'))
    //         array.forEach(function (generalDoctor) {
    //             creatDivForCard(generalDoctor)
    //         })
    //     }
    // }

    // crossImg2.addEventListener('click', (event) => {
    //     divForCard.remove()
    //     let card = document.querySelectorAll('.divForCard');
    //     let currentIndex = 0;
    //     card.forEach(function (element, index) {
    //         if (event.target.parentNode === element)
    //             currentIndex = index;
    //     });
    //     array.splice(currentIndex, 1);
    //     console.log(array);
    //
    //     localStorage.setItem('generalDoctor', JSON.stringify(array));
    //     if (array.length === 0 ){
    //         // card.style.display = 'block'
    //     }
    // })

    localStorage.setItem(`${Math.random()}`,divForCard.outerHTML);
}

inputCreat.addEventListener('click', () => {
    let generalDoctor;
    coverDiv.style.display = 'none'
    let backgroundContainerText = document.getElementById('backgroundContainerText')
    backgroundContainerText.innerText = ''
    if (inputDoctorName.value === 'Выберети специалиста') {
        removeAllInput()
        coverDiv.style.display = 'block'
    } else if (inputDoctorName.value === 'Кардиолог') {
        generalDoctor = new Cardiologist()
    } else if (inputDoctorName.value === 'Стоматолог') {
        generalDoctor = new Dentist()
    } else if (inputDoctorName.value === 'Терапевт') {
        generalDoctor = new Therapist()
    }
    array.push(generalDoctor);
    // localStorage.setItem('generalDoctor',JSON.stringify(array));
    console.log(array);
    // console.log(array);
    creatDivForCard(generalDoctor);

    window.onload = function () {
        // if (localStorage.getItem('generalDoctor')){
        //         //     array = JSON.parse(localStorage.getItem('generalDoctor'));
        //         //     console.log(array);
        //         //     array.forEach(function (generalDoctor) {
        //         //         creatDivForCard(generalDoctor)
        //         //     })
        //         // }
        let length = localStorage.length;
        if(length){
            for(let i=0;i<length;i++){
                let key = localStorage.key(i);
                let item = localStorage.getItem(key);
                document.getElementById('backgroundContainerText').innerHTML+=item;

            }
        }
    };

})

function onLoad() {
    let length = localStorage.length;
    console.log(length)
    if(length){
        document.getElementById('backgroundContainerText').style.display='none';
        for(let i=0;i<length;i++){
            let key = localStorage.key(i);
            let item = localStorage.getItem(key);
            document.getElementsByClassName('backgroundContainer')[0].innerHTML+=item;
        }
    }
}

onLoad();
